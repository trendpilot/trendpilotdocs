---
sidebar_position: 2
---

# API Project

The API was built with ASP.NET.

## Project Architecture

The projects architecture is inspired by Clean Architecture principles. I didn't see a need at this time to break out "Application" and "Domain" modules at this time. Instead both concepts exist in the "Core" module. Dependency Inversion is utilized to separate the core module from infrastructure implementations.

```mermaid
---
title: API Components
---
flowchart TD
    api["API"]
    core["Application Core"]
    infra["Infrastructure"]
    api --> core
    infra --> core
    api --> infra
```

### Components

#### API

Handles client queries and executes commands.

#### Application Core

Implements the business logic. The Application Core project _is_ the application.

#### Infrastructure

Implements the necessary "Infrastructure" interfaces contained in the Application Core project. These implementations allow the application to interact with other services (e.g. databases, caches, data providers).