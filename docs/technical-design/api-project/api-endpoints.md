---
sidebar_position: 3
---

# API Endpoints

## URL Structure Summary

### User Routes

I'm choosing to prefix these endpoints with a 'user' slug since they are executed in the context of the requesting user. Probably determined from the JWT token. Futhermore, they are split into 'commands' and 'queries'. This may not be necessary since you can get an idea from the HTTP verb. I'm not really attempting to be RESTful since these endpoints are tailor-made for use cases/application functionality. Applying RESTful principles to these might lead to unecessary complexity and be counter-productive. Sticking to an RPC-style interaction removes developer desire to segregate entities in a RESTful manner.

```
POST /user/command/start-campaign
POST /user/command/add-campaign-position
POST /user/command/add-maintenance-config
GET  /user/query/portfolio-report
GET  /user/query/campaign/{id}
```

### Market Data Routes

Attempting to follow a more RESTful structure with these URLs even though other REST features (e.g. HATEOAS) won't be implemented since we can't modify quotes or derivative data. 

```
GET  /marketdata/{symbol}/quote
GET  /marketdata/{symbol}/option-chains?expiration=20240419
```

## Endpoint Detail

### Start Campaign Endpoint
```
Action: POST
Url: /user/command/start-campaign
Authentication: Required

Request
---------------
Headers:
- Authorization: Bearer <JWT>

Payload:
{
  symbol: NVDA,
  positions: [
    {
      assetType: Stock/Option,
      symbol: NVDA,
      quantity: 100
    }
  ]
}

Response 
---------------
Headers:
- Location: <URL>

Payload:
<empty>
```

### Add Campaign Postion Endpoint
```
Action: POST
Url: /user/command/add-campaign-position
Authentication: Required

Request
---------------
Headers:
- Authorization: Bearer <JWT>

Payload:
{
  assetType: Stock/Option,
  symbol: NVDA,
  quantity: 100
}

Response 
---------------
Headers:
- Location: <URL>

Payload:
<empty>
```

### Get Campaigns Endpoint
```
Action: Get
Url: user/query/portfolio-report
Authentication: Required

Request
---------------
Headers:
- Authorization: Bearer <JWT>

Payload:
<empty>

Response 
---------------
Headers:
- Location: <URL>

Payload:
{
  positions: [
    {
      symbol: NVDA,
      quantity: 100,
      tradePrice: 903.21,
      calls: [
        {
          symbol: NVDA24041901000000,
          quantity: 1,
          tradePrice: 1756,
          strike: 1000,
          expirations: 20240419
        }
      ]
    },
  ]
}
```