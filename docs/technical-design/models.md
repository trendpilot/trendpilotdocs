---
sidebar_position: 4
---

# Models

## Domain Model

A few things I'm not happy with. I guess this is a starting point.

```mermaid
---
title: Domain Model
---
erDiagram
    Position ||--|| Asset : "provides exposure to"
    Quote ||--|| Asset : references
    Campaign ||--|{ Position : aggregates
    Campaign ||--|{ Quote : uses
    OptionChain ||--o{ Quote : contains
```

## Read Models

TBD...