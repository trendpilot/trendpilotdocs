---
sidebar_position: 1
---

# System Architecture

Information and diagrams outlining the components of the TrendPilot system.

## System Overview

```mermaid
---
title: System Component Overview
---
flowchart TD;
    user["User (Browser)"];
    remix["TrendPilot Front-End (Remix JS)"]
    api["TrendPilot API (ASP.NET)"]
    fn["Position Monitor (Azure Fn)"]
    db["TrendPilot DB (CosmosDB)"]
    marketdata["3rd-Party Market Data"]
    user --> remix
    remix --> api
    fn --> api
    api --> db
    api --> marketdata
```

### Components

#### TrendPilot Front-End

The front-end is a Remix JS project.

#### TrendPilot API

TrendPilot API is an ASP.NET project.