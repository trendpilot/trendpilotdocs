---
sidebar_position: 1
---

# About

Learn about TrendPilots' use cases and objectives.

## Mission

TrendPilot's mission is to generate passive income from your existing stock and ETF holdings.

## Who is this for?

TrendPilot is for the typical buy-and-hold investor. Covered-call campaigns need time to work and can underperform buy-and-hold in the short-term. The stocks/ETFs used as underlyings will not be sold.

## How does it work?

TrendPilot will aid in managing the sale of call option contracts against existing stock and ETF positions with the goal of earning passive income from the eroding value of the short contracts. These are known in the investment community as "covered-calls". TrendPilot will monitor open contracts and take action based on the underlyings' price action. Of course like any bet, this doesn't always go as planned. In the case the underlyings' price increases drastically the short contract will gain value (bad for you since you've already sold at a lower price). In this case, TrendPilot will "roll" the contract "up-and-out". This means it will buy-to-close the open contract and re-sell another contract with an expiration further in the future and further from the current asset price. The result is your portfolio value will be less volatile, and you'll earn additional income from the eroding of the covered-calls.

## Which assets work best?

Assets with a high Implied Volatility (IV) rating work best. Option contracts are priced according to an assets potential to move higher. IV is a measure of that potential. So the higher your assets' IV, the more valuable the call contracts will be. Since you're selling the call contract, the higher sale price means more potential income for you, if the contract expires worthless.