---
sidebar_position: 1
---

# Use Cases

## Start a New Campaign

Following successful login, the user has the ability to start a new covered-call campaign. The user may also provide one or more new positions to open.

Required API Endpoint(s):
- [Start Campaign Endpoint](/docs/technical-design/api-project/api-endpoints#start-campaign-endpoint)

## Open a New Campaign Position

Following successful login, the user has the ability to open a new stock/ETF position (i.e. buy stock). The user provides the ticker symbol and quantity. The current asset price is queried and utilized as the trade price for the position.

Required API Endpoint(s):
- [Open Position Endpoint](/docs/technical-design/api-project/api-endpoints#add-campaign-postion-endpoint)

## Monitor Existing Campaigns

Following successful login, the user has the ability to see existing positions and the profit/loss incurred.

Required API Endpoint(s):
- [Get User Campaigns](/docs/technical-design/api-project/api-endpoints#get-campaigns-endpoint)

## Close an Existing Campaign Position

TBD...

## Sell-to-Open a Covered Call

TBD...

## Roll a Covered Call

TBD...

## Buy-to-Close a Covered Call

TBD...

## Configure Automated Covered Call Management

TBD...